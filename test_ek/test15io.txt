[
    15 10
    8 5
]
DECLARE
    a, b, n
BEGIN
    READ a;
    READ b;
    n := 10;
    IF a < b THEN
        READ n;
        n := 3;
    ELSE
        n := 8;
        WRITE n;
        n := 5;
    ENDIF

    WRITE n;
END
