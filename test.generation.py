import random

def mod(a, b):
    return a % b if b != 0 else 0

def div(a, b):
    return a // b if b != 0 else 0

def sub(a, b):
    return max(0, a-b)


for i in range(50):
    with open(f'./arytmetyka_tabs/knowns_v{str(i).zfill(3)}.test', "wb") as file:
        a = random.randint(0, 1024*1024)
        b = random.randint(0, 1024*1024)
        x = random.randint(0, 1024*1024)
        z = 0
        w = 1
        n = random.randint(1, 1024*1024)
        content = f'''[
    {n} {a} {b} {x} {z} {w}
    {div(a, b)} {a * b} {a + b} {sub(a, b)} {mod(a, b)} {div(a, x)} {a * x} {a + x} {sub(a, x)} {mod(a, x)} {div(b, x)} {b * x} {b + x} {sub(b, x)} {mod(b, x)} {div(b, z)} {b * z} {b + z} {sub(b, z)} {mod(b, z)} {div(b, w)} {b * w} {b + w} {sub(b, w)} {mod(b, w)} {a} {b} {x}
    ]
    DECLARE
        a({n-1}:{n+1}), b({n-1}:{n+1}), x({n-1}:{n+1}), c({n-1}:{n+1}), z({n-1}:{n+1}), w({n-1}:{n+1}), n
    BEGIN
        READ n;
        READ a({n});
        READ b({n});
        READ x({n});
        READ z({n});
        READ w({n});

        c({n}) := a({n}) / b({n});
        WRITE c({n});

        c({n}) := a({n}) * b({n});
        WRITE c({n});

        c({n}) := a({n}) + b({n});
        WRITE c({n});

        c({n}) := a({n}) - b({n});
        WRITE c({n});

        c({n}) := a({n}) % b({n});
        WRITE c({n});



        c({n}) := a({n}) / x({n});
        WRITE c({n});

        c({n}) := a({n}) * x({n});
        WRITE c({n});

        c({n}) := a({n}) + x({n});
        WRITE c({n});

        c({n}) := a({n}) - x({n});
        WRITE c({n});

        c({n}) := a({n}) % x({n});
        WRITE c({n});



        c({n}) := b({n}) / x({n});
        WRITE c({n});

        c({n}) := b({n}) * x({n});
        WRITE c({n});

        c({n}) := b({n}) + x({n});
        WRITE c({n});

        c({n}) := b({n}) - x({n});
        WRITE c({n});

        c({n}) := b({n}) % x({n});
        WRITE c({n});

        

        c({n}) := b({n}) / z({n});
        WRITE c({n});

        c({n}) := b({n}) * z({n});
        WRITE c({n});

        c({n}) := b({n}) + z({n});
        WRITE c({n});

        c({n}) := b({n}) - z({n});
        WRITE c({n});

        c({n}) := b({n}) % z({n});
        WRITE c({n});


        c({n}) := b({n}) / w({n});
        WRITE c({n});

        c({n}) := b({n}) * w({n});
        WRITE c({n});

        c({n}) := b({n}) + w({n});
        WRITE c({n});

        c({n}) := b({n}) - w({n});
        WRITE c({n});

        c({n}) := b({n}) % w({n});
        WRITE c({n});


        WRITE a({n});
        WRITE b({n});
        WRITE x({n});
    END
    '''
        file.write(content.encode())

    