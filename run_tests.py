import re
import os
import sys
import glob
import time
import pickle
import subprocess
from colorama import Fore
from pathlib import Path

show_compile_logs = False
best_scores_path = 'bests.pickle'

if os.path.isfile(best_scores_path):
    with open(best_scores_path, "rb") as file:
        best_scores = pickle.load(file)
else:
    best_scores = {}

print("Compiling compilator...")
subprocess.run(['bash', './compile_compiler.sh'], stdout=subprocess.DEVNULL)

if not os.path.isdir("./compiled"):
    os.mkdir("./compiled")

if len(sys.argv) < 3:
    print("Podaj ścieżkę do vmki i foldery z testami jako argument")
    exit(0)

tests_passed = True
override_pickle = True

vm_path = sys.argv[1]

for filepath in sorted(glob.glob('./compiled/*')):
    os.remove(filepath)

for test_folder in sys.argv[2:]:
    print(Fore.BLUE + f'Running tests in folder {test_folder}' + Fore.RESET)

    for filepath in sorted(glob.glob(f'./{test_folder}/*')):
        inputs, expected_outputs = open(filepath, 'r+').readlines()[1:3]
        inputs = inputs.split()
        expected_outputs = expected_outputs.split()

        compiled_filename = f"./compiled/{Path(filepath).name}"

        print("{:<55}".format(f"Compiling {filepath}"), end=' ')
        start_time = time.time()
        compilation = subprocess.run(['bash', './compile.sh', filepath, compiled_filename],
            text=True, capture_output=True, timeout=180
        )
        if show_compile_logs:
            print(compilation.stdout)

        if os.path.isfile(compiled_filename):
            compilation_time = time.time() - start_time
            if compilation_time < 1.0:
                compilation_color = Fore.GREEN
            elif compilation_time < 3.0:
                compilation_color = Fore.BLUE
            elif compilation_time < 4.0:
                compilation_color = Fore.YELLOW
            elif compilation_time < 5.0:
                compilation_color = Fore.MAGENTA
            else:
                compilation_color = Fore.RED

            print(compilation_color + "Done. {:>8} ".format(f"{round(compilation_time, 2)}s") + Fore.RESET + "Running...", end=' ')

            vm_output = subprocess.run([vm_path, compiled_filename],
                text=True,
                capture_output=True,
                input='\n'.join(inputs),
                timeout=30
            )

            output = vm_output.stdout.strip().replace('?', '\n')

            outputs = [line.strip()[2:] for line in output.splitlines() if line.strip().startswith('>')]
            cost = int(re.findall('[0-9]+', output.splitlines()[-1])[1])

            if len(expected_outputs) == len(outputs) and all([a==b for a, b in zip(expected_outputs, outputs)]):
                previous_score = best_scores.get(filepath, 999999999999)
                print(Fore.GREEN + f"Test ok! {Fore.RESET} Cost: {Fore.YELLOW if cost > previous_score else (Fore.GREEN if cost < previous_score else Fore.BLUE)} {cost:>10} {Fore.RESET} Previous best: {previous_score:>10}" + Fore.RESET)
                if cost < previous_score:
                    best_scores[filepath] = cost
            else:
                print(Fore.RED + f"Test {filepath} failed!")
                print(f"Expected output: {expected_outputs}")
                print(f"Program output: {outputs}" + Fore.RESET)
                tests_passed = False
        else:
            print(Fore.RED + f"Test {filepath} failed! Compiler didn't compile code!" + Fore.RESET)
            tests_passed = False

if tests_passed:
    print(f"\n{Fore.GREEN}Test passed! {Fore.RESET}")
    if override_pickle:
        with open(best_scores_path, "wb") as file:
            pickle.dump(best_scores, file)
else:
    print(f"\n{Fore.RED}Test failed! {Fore.RESET}")
